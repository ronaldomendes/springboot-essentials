package com.cursospring.springbootessentials.javaclient;

import com.cursospring.springbootessentials.handler.RestResponseExceptionHandler;
import com.cursospring.springbootessentials.model.PageableResponse;
import com.cursospring.springbootessentials.model.Student;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

public class JavaClientDAO {

    private final RestTemplate restTemplate = new RestTemplateBuilder()
            .rootUri("http://localhost:8080/v1/user/students")
            .basicAuthentication("venom", "senha123")
            .errorHandler(new RestResponseExceptionHandler())
            .build();

    private final RestTemplate restTemplateAdmin = new RestTemplateBuilder()
            .rootUri("http://localhost:8080/v1/admin/students")
            .basicAuthentication("spidey", "senha123")
            .errorHandler(new RestResponseExceptionHandler())
            .build();

    private static HttpHeaders createJsonHeader() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        return headers;
    }

    /**
     * No método get também é possível usar um rest template com getForObject ou getForEntity
     */
    public Student findById(Long id) {
        return restTemplate.getForObject("/{id}", Student.class, id);
    }

    public List<Student> listAll() {
        ResponseEntity<PageableResponse<Student>> exchange = restTemplate.exchange("/", HttpMethod.GET, null,
                new ParameterizedTypeReference<>() {
                });
        if (exchange.getBody() == null) {
            return new ArrayList<>();
        }
        return exchange.getBody().getContent();
    }

    /**
     * No método post também é possível usar um rest template com postForObject ou postForEntity
     */
    public Student save(Student student) {
        ResponseEntity<Student> exchange = restTemplateAdmin.exchange("/", HttpMethod.POST,
                new HttpEntity<>(student, createJsonHeader()), Student.class);
        return exchange.getBody();
    }

    public void update(Student student) {
        restTemplateAdmin.put("/", student);
    }

    public void delete(Long id) {
        restTemplateAdmin.delete("/{id}", id);
    }

}
