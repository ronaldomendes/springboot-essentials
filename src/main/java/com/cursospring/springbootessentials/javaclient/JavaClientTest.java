package com.cursospring.springbootessentials.javaclient;

import org.apache.tomcat.util.codec.binary.Base64;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.web.bind.annotation.RequestMethod.GET;

public class JavaClientTest {
    public static void main(String[] args) {
        HttpURLConnection connection = null;
        BufferedReader reader = null;
        String user = "venom";
        String senha = "senha123";
        try {
            URL url = new URL("http://localhost:8080/v1/user/students/4");
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod(GET.name());
            connection.addRequestProperty(AUTHORIZATION, "Basic " + encodeUsernamePassword(user, senha));
            reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            StringBuilder response = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                response.append(line);
            }
            System.out.println(response.toString());
        } catch (Exception e) {
            e.printStackTrace();
            IOUtils.closeQuietly(reader);
            if (connection != null) {
                connection.disconnect();
            }
        }
    }

    private static String encodeUsernamePassword(String user, String password) {
        String userAndPassword = user + ":" + password;
        return new String(Base64.encodeBase64(userAndPassword.getBytes()));
    }
}
