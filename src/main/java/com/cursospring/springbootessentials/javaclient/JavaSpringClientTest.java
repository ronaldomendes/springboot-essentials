package com.cursospring.springbootessentials.javaclient;

import com.cursospring.springbootessentials.model.Student;

public class JavaSpringClientTest {
    public static void main(String[] args) {

        Student studentPost = new Student();
        studentPost.setName("Alex Wesker");
        studentPost.setEmail("alex.wesker@email.com");

        Student studentUpdate = new Student();
        studentUpdate.setId(3L);
        studentUpdate.setName("Albert Wesker");
        studentUpdate.setEmail("albert.wesker@email.com");

        JavaClientDAO dao = new JavaClientDAO();
        System.out.println(dao.findById(6L));
//        System.out.println(dao.listAll());
//        System.out.println(dao.save(studentPost));
//        dao.update(studentUpdate);
//        System.out.println(dao.findById(66L));
        dao.delete(99L);
    }
}
