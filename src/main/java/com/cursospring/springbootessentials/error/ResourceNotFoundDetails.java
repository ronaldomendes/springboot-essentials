package com.cursospring.springbootessentials.error;

public class ResourceNotFoundDetails extends ErrorDetails {


    public static final class Builder {
        private String title;
        private int status;
        private String detail;
        private long timestamp;
        private String devMessage;

        private Builder() {
        }

        public static Builder newBuilder() {
            return new Builder();
        }

        public Builder title(String title) {
            this.title = title;
            return this;
        }

        public Builder status(int status) {
            this.status = status;
            return this;
        }

        public Builder detail(String detail) {
            this.detail = detail;
            return this;
        }

        public Builder timestamp(long timestamp) {
            this.timestamp = timestamp;
            return this;
        }

        public Builder devMessage(String devMessage) {
            this.devMessage = devMessage;
            return this;
        }

        public ResourceNotFoundDetails build() {
            ResourceNotFoundDetails resourceNotFoundDetails = new ResourceNotFoundDetails();
            resourceNotFoundDetails.setTimestamp(timestamp);
            resourceNotFoundDetails.setDevMessage(devMessage);
            resourceNotFoundDetails.setStatus(status);
            resourceNotFoundDetails.setTitle(title);
            resourceNotFoundDetails.setDetail(detail);
            return resourceNotFoundDetails;
        }
    }
}
