package com.cursospring.springbootessentials.endpoint;


import com.cursospring.springbootessentials.error.ResourceNotFoundException;
import com.cursospring.springbootessentials.model.Student;
import com.cursospring.springbootessentials.repository.StudentRepository;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Optional;

@RestController
@RequestMapping("/v1")
public class StudentEndpoint {

    private final StudentRepository repository;

    public StudentEndpoint(StudentRepository repository) {
        this.repository = repository;
    }


    @GetMapping(path = "/user/students")
    @ApiOperation(value = "Retorna uma lista de estudantes", response = Student[].class)
    public ResponseEntity<?> listAll(Pageable pageable) {
        return new ResponseEntity<>(repository.findAll(pageable), HttpStatus.OK);
    }

    @GetMapping(path = "/user/students/{id}")
    @ApiOperation(value = "Retorna um estudante através da consulta por id", response = Student.class)
    public ResponseEntity<?> getStudentById(@PathVariable("id") Long id, Authentication auth) {
        System.out.println(auth);
        Optional<Student> student = Optional.ofNullable(repository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Student not found for ID: " + id)));
        return new ResponseEntity<>(student, HttpStatus.OK);
    }

    @GetMapping(path = "/user/students/findByName/{name}")
    @ApiOperation(value = "Retorna um estudante através da consulta por nome", response = Student.class)
    public ResponseEntity<?> getStudentsByName(@PathVariable("name") String name) {
        return new ResponseEntity<>(repository.findByNameIgnoreCaseContaining(name), HttpStatus.OK);
    }

    @PostMapping(path = "/admin/students")
    @Transactional(rollbackFor = Exception.class)
    @ApiOperation(value = "Cadastra um estudante", response = Student.class)
    public ResponseEntity<?> save(@Valid @RequestBody Student student) {
        return new ResponseEntity<>(repository.save(student), HttpStatus.CREATED);
    }

    @DeleteMapping(path = "/admin/students/{id}")
    @PreAuthorize(value = "hasRole('ADMIN')")
    @ApiOperation(value = "Remove um estudante através do id")
    public ResponseEntity<?> delete(@PathVariable Long id) {
        verifyIfStudentExists(id);
        repository.deleteById(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @PutMapping(path = "/admin/students")
    @ApiOperation(value = "Atualiza os dados de um estudante")
    public ResponseEntity<?> update(@RequestBody Student student) {
        verifyIfStudentExists(student.getId());
        repository.save(student);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    private void verifyIfStudentExists(Long id) {
        repository.findById(id).orElseThrow(
                () -> new ResourceNotFoundException("Student not found for ID: " + id));
    }
}
