package com.cursospring.springbootessentials.repository;

import com.cursospring.springbootessentials.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {
    User findByUsername(String name);
}
