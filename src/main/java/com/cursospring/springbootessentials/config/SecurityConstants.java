package com.cursospring.springbootessentials.config;

public class SecurityConstants {
    public static final String SECRET = "fA2j398nsdfup!23";
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String SIGN_UP_URL = "/users/sign-up";
    public static final Long EXPIRATION_TIME = 86400000L;
}
