package com.cursospring.springbootessentials.endpoint;

import com.cursospring.springbootessentials.model.Student;
import com.cursospring.springbootessentials.repository.StudentRepository;
import com.cursospring.springbootessentials.repository.UserRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.http.*;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.client.ResourceAccessException;

import java.util.List;

import static java.util.Arrays.asList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
public class StudentEndpointTest {

    @Autowired
    private TestRestTemplate restTemplate;

    private MockRestServiceServer mockServer;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private StudentRepository studentRepository;

    @MockBean
    private UserRepository userRepository;

    @TestConfiguration
    static class Config {
        @Bean
        public RestTemplateBuilder restTemplateBuilder() {
            return new RestTemplateBuilder().basicAuthentication("spidey", "senha123");
        }
    }

    @Test
    public void whenListStudentUsingIncorrectUsernameAndPassword_thenReturnStatusCode401Unauthorized() {
        restTemplate = restTemplate.withBasicAuth("1", "1");
        ResponseEntity<String> response = restTemplate.getForEntity("/v1/user/students/", String.class);
        assertThat(response.getStatusCodeValue()).isEqualTo(401);
    }

    @Test
    public void whenGetStudentByIdUsingIncorrectUsernameAndPassword_thenReturnStatusCode401Unauthorized() {
        restTemplate = restTemplate.withBasicAuth("1", "1");
        ResponseEntity<String> response = restTemplate.getForEntity("/v1/user/students/1", String.class);
        assertThat(response.getStatusCodeValue()).isEqualTo(401);
    }

    @Test
    public void whenFindStudentByNameUsingIncorrectUsernameAndPassword_thenReturnStatusCode401Unauthorized() {
        restTemplate = restTemplate.withBasicAuth("1", "1");
        ResponseEntity<String> response = restTemplate.getForEntity("/v1/user/students/findByName/studentName", String.class);
        assertThat(response.getStatusCodeValue()).isEqualTo(401);
    }

    @Test
    public void whenSaveStudentUsingIncorrectUsernameAndPassword_thenReturnResourceAccessException() {
        restTemplate = restTemplate.withBasicAuth("1", "1");
        Student student = new Student(1L, "Legolas", "legolas@lotr.com");
        assertThrows(ResourceAccessException.class, () -> restTemplate.postForEntity("/v1/admin/students/", student, String.class));
    }

    @Test
    public void whenDeleteStudentUsingIncorrectUsernameAndPassword_thenReturnStatusCode401Unauthorized() {
        restTemplate = restTemplate.withBasicAuth("1", "1");
        ResponseEntity<String> response = restTemplate.exchange("/v1/admin/students/{id}",
                HttpMethod.DELETE, null, String.class, 1L);
        assertThat(response.getStatusCodeValue()).isEqualTo(401);
    }

    @Test
    public void whenUpdateStudentUsingIncorrectUsernameAndPassword_thenReturnResourceAccessException() {
        restTemplate = restTemplate.withBasicAuth("1", "1");
        Student student = new Student(1L, "Legolas", "legolas@lotr.com");
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<Student> entity = new HttpEntity<>(student, headers);
        assertThrows(ResourceAccessException.class, () -> restTemplate.exchange("/v1/admin/students",
                HttpMethod.PUT, entity, String.class, 1L));
    }


    @Test
    @WithMockUser(username = "xxx", password = "xxx", roles = "USER")
    public void whenListAllStudentsUsingCorrectRole_thenReturnStatusCode200() throws Exception {
        List<Student> students = asList(new Student(1L, "Legolas", "legolas@lotr.com"),
                new Student(2L, "Aragorn", "aragorn@lotr.com"));
        when(studentRepository.findAll()).thenReturn(students);
        mockMvc.perform(get("/v1/user/students/")).andExpect(status().isOk());
    }

    @Test
    @WithMockUser(username = "xxx", password = "xxx", roles = {"USER"})
    public void whenGetStudentByIdUsingCorrectRoleAndStudentDoesntExist_thenReturnStatusCode404() throws Exception {
        Student student = new Student(3L, "Legolas", "legolas@lotr.com");
        when(studentRepository.findById(3L)).thenReturn(java.util.Optional.of(student));
        mockMvc.perform(get("/v1/user/students/{id}", 6))
                .andExpect(status().isNotFound());
        verify(studentRepository).findById(6L);
    }

    @Test
    @WithMockUser(username = "xxx", password = "xxx", roles = {"USER"})
    public void whenFindStudentsByNameUsingCorrectRole_thenReturnStatusCode200() throws Exception {
        List<Student> students = asList(new Student(1L, "Legolas", "legolas@lotr.com"),
                new Student(2L, "Aragorn", "aragorn@lotr.com"),
                new Student(3L, "legolas greenleaf", "legolas.gl@lotr.com"));
        when(studentRepository.findByNameIgnoreCaseContaining("legolas")).thenReturn(students);
        mockMvc.perform(get("/v1/user/students/findByName/legolas"))
                .andExpect(status().isOk());
        verify(studentRepository).findByNameIgnoreCaseContaining("legolas");
    }

    @Test
    @WithMockUser(username = "xxx", password = "xxx", roles = {"ADMIN"})
    public void whenDeleteHasRoleAdminAndStudentDontExist_thenReturnStatusCode404() throws Exception {
        doNothing().when(studentRepository).deleteById(1L);
        mockMvc.perform(delete("/v1/admin/students/{id}", 1))
                .andExpect(status().isNotFound());
        verify(studentRepository, atLeast(1)).findById(1L);
    }

    @Test
    @WithMockUser(username = "xxx", password = "xxx", roles = {"USER"})
    public void whenDeleteHasRoleUser_thenReturnStatusCode403() throws Exception {
        doNothing().when(studentRepository).deleteById(1L);
        mockMvc.perform(delete("/v1/admin/students/{id}", 1))
                .andExpect(status().isForbidden());
    }
}
