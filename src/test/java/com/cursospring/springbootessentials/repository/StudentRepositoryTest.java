package com.cursospring.springbootessentials.repository;

import com.cursospring.springbootessentials.model.Student;
import com.cursospring.springbootessentials.repository.StudentRepository;
import org.hibernate.exception.ConstraintViolationException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(SpringExtension.class)
@DataJpaTest
//@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE) // para testar com o banco de dados
public class StudentRepositoryTest {

    @Autowired
    private StudentRepository repository;

    @Test
    public void createShouldPersistData() {
        Student s01 = new Student("Jill Valentine", "jill.valentine@email.com");
        repository.save(s01);
        assertThat(s01.getId()).isNotNull();
        assertThat(s01.getName()).isEqualTo("Jill Valentine");
        assertThat(s01.getEmail()).isEqualTo("jill.valentine@email.com");
    }

    @Test
    public void deleteShouldRemoveData() {
        Student s01 = new Student("Jill Valentine", "jill.valentine@email.com");
        repository.save(s01);
        repository.deleteById(s01.getId());
        assertThat(repository.findById(s01.getId())).isEmpty();
    }

    @Test
    public void updateShouldChangeAndPersistData() {
        Student s01 = new Student("Jill Valentine", "jill.valentine@email.com");
        repository.save(s01);

        s01 = new Student("Ms. Jill Valentine", "ms.valentine@email.com");
        repository.save(s01);

        s01 = repository.findById(s01.getId()).orElse(null);
        assertThat(s01.getName()).isEqualTo("Ms. Jill Valentine");
        assertThat(s01.getEmail()).isEqualTo("ms.valentine@email.com");
    }

    @Test
    public void findByNameIgnoreCaseContainingShouldIgnoreCase() {
        Student s01 = new Student("Jill Valentine", "jill.valentine@email.com");
        Student s02 = new Student("jill valentine", "ms.valentine@email.com");
        repository.saveAll(Arrays.asList(s01, s02));

        List<Student> list = repository.findByNameIgnoreCaseContaining("jill Valentine");
        assertThat(list.size()).isEqualTo(2);
    }

//    @Test
    public void createWhenNameIsNullShouldThrowsConstraintViolationException() {
        Exception exception = assertThrows(ConstraintViolationException.class,
                () -> repository.save(new Student(null, "ms.valentine@email.com")));
        assertTrue(exception.getMessage().contains("O nome é obrigatório!"));
    }

//    @Test
    public void createWhenEmailIsNullShouldThrowsConstraintViolationException() {
        Exception exception = assertThrows(ConstraintViolationException.class,
                () -> repository.save(new Student("Jill Valentine", null)));
        assertTrue(exception.getMessage().contains("O e-mail é obrigatório!"));
    }

//    @Test
    public void createWhenEmailIsInvalidThrowsConstraintViolationException() {
        Exception exception = assertThrows(ConstraintViolationException.class,
                () -> repository.save(new Student("Jill Valentine", "jill")));
        assertTrue(exception.getMessage().contains("Formato de e-mail inválido!"));
    }

}
